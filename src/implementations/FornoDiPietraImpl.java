package implementations;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;

import enums.PizzaIngredient;
import interfaces.FornoDiPietraInterface;
import models.Order;
import models.Pizza;

public class FornoDiPietraImpl implements FornoDiPietraInterface, Serializable {
	
	private static final long serialVersionUID = -3549440015100397269L;
	@SuppressWarnings("unused")
	private ArrayList<Order> orders;
	
	public FornoDiPietraImpl() throws RemoteException {
		this.orders = new ArrayList<>();
	}

	@Override
	public float addOrder(Order order) throws RemoteException {
		float price = 0;
		Pizza pizza = null;
		
		if(order != null) {
			pizza = order.getPizza();
			
			if(pizza != null)
				price += pizza.getSize().getPrice();
				price += pizza.getMass().getPrice();
				for(PizzaIngredient ingredient : pizza.getIngredients())
					price += ingredient.getPrice();
		}
		
		return price;
	}

}
