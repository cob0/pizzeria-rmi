package controllers;

import java.util.Scanner;

import models.Order;

public class OrderController {

	private Order order;

	public OrderController(Order order) {
		this.setOrder(order);
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
	public void readName() {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Name of contact: ");
			order.setName(teclado.nextLine());
		} while(order.getName().isEmpty());
	}
	
	public void readStreet() {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Street to send: ");
			order.setStreet(teclado.nextLine());
		} while(order.getStreet().isEmpty());
	}
	
	public void readContact() {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Tlf.: ");
			order.setContact(teclado.nextLine());
		} while(order.getContact().isEmpty());
	}
	
	public void readPizza() {
		new PizzaController(order.getPizza()).readData();
	}
	
	public void readData() {
		readName();
		readStreet();
		readContact();
		readPizza();
	}
	
}
