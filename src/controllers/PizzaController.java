package controllers;

import java.util.InputMismatchException;
import java.util.Scanner;

import enums.PizzaIngredient;
import enums.PizzaMass;
import enums.PizzaSize;
import models.Pizza;

public class PizzaController {
	
	private Pizza pizza;
	
	public PizzaController(Pizza pizza) {
		this.setPizza(pizza);
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}
	
	public void readSize() {
		PizzaSize[] sizes = PizzaSize.values();
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.println("PIZZA SIZES: ");
			for(PizzaSize size : sizes)
				System.out.println(size.getSize());
			System.out.print("Write your option: ");
			try {
				pizza.setSize(PizzaSize.valueOf(teclado.nextLine().toUpperCase()));
			} catch(IllegalArgumentException e) {
				System.err.println("This size doesn't exist.");
			}
		} while(pizza.getSize() == null);
	}
	
	public void readMass() {
		PizzaMass[] mass = PizzaMass.values();
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.println("PIZZA MASS: ");
			for(PizzaMass m : mass)
				System.out.println(m.getMass());
			
			System.out.print("Write your option: ");
			try {
				pizza.setMass(PizzaMass.valueOf(teclado.nextLine().replaceAll(" ", "_").toUpperCase()));
			} catch(IllegalArgumentException e) {
				System.err.println("This mass doesn't exist.");
			}
		} while(pizza.getMass() == null);
	}
	
	public PizzaIngredient readIngredient() {
		PizzaIngredient ingredient = null;
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Ingredient: ");
			try {
				ingredient = PizzaIngredient.valueOf(teclado.nextLine().replaceAll(" ", "_").toUpperCase());
			} catch(IllegalArgumentException e) {
				System.err.println("This ingredient doesn't exist.");
			}
		} while(ingredient == null);
		
		return ingredient;
	}
	
	public void readIngredients() {
		PizzaIngredient[] ingredients = PizzaIngredient.values();
		int numIngredients = 0;
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("How many ingredients? ");
			try {
				numIngredients = teclado.nextInt();
			} catch(InputMismatchException e) {
				System.err.println("Write only numbers.");
			}
		} while(numIngredients < 1);
		
		System.out.println("INGREDIENTS:");
		for(PizzaIngredient ingredient : ingredients)
			System.out.println(ingredient.getIngredient());
		
		for(int i = numIngredients; i > 0; i--)
			pizza.getIngredients().add(readIngredient());
	}
	
	public void readData() {
		readSize();
		readMass();
		readIngredients();
	}

}
