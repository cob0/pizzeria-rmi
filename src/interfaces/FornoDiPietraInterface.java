package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import models.Order;

public interface FornoDiPietraInterface extends Remote {
	
	float addOrder(Order order) throws RemoteException;

}
