package enums;

public enum PizzaSize {
	SMALL("small", 0.5f),
	MEDIUM("medium", 1f),
	BIG("big", 3);
	
	private String size;
	private float price;
	
	PizzaSize(String size, float price) {
		this.size = size;
		this.price = price;
	}
	
	public String getSize() {
		return size;
	}
	
	public float getPrice() {
		return price;
	}
}
