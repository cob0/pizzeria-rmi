package enums;

public enum PizzaIngredient {
	TOMATO("tomato", 3f),
	CHEESE("cheese", 3f),
	BACON("bacon", 5f),
	OLIVE("olive", 5.5f),
	ANCHOVY("anchovy", 6.5f),
	CHICKEN("chicken", 4.5f),
	MINCE("mince", 4f),
	RED_PEPPER("red pepper", 2.5f),
	GREEN_PEPPER("green pepper", 2f);
	
	private String ingredient;
	private float price;
	
	PizzaIngredient(String ingredient, float price) {
		this.ingredient = ingredient;
		this.price = price;
	}
	
	public String getIngredient() {
		return ingredient;
	}

	public float getPrice() {
		return price;
	}
}
