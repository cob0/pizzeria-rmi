package enums;

public enum PizzaMass {
	CLASSIC("classic", 2f),
	THREE_FLOORS("three floors", 6f),
	THIN("thin", 1.5f);
	
	private String mass;
	private float price;
	
	PizzaMass(String mass, float price) {
		this.mass = mass;
		this.price = price;
	}
	
	public String getMass() {
		return mass;
	}
	
	public float getPrice() {
		return price;
	}
}
