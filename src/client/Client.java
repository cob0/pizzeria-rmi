package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import controllers.OrderController;
import interfaces.FornoDiPietraInterface;
import models.Order;

public class Client {
	
	public static void main(String[] args) {
		String nombreServicio;		
		Registry registry;
		FornoDiPietraInterface forno;
		OrderController orderController = null;
		float total;
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		

		try {
			registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido.");
			
			nombreServicio = "Forno";
						
			forno = (FornoDiPietraInterface) registry.lookup(nombreServicio);
			orderController = new OrderController(new Order());
			orderController.readData();
			total = forno.addOrder(orderController.getOrder());
			
			System.out.println("Total a pagar: " + total + "�");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Saliendo del programa...");
	}

}
