package models;

import java.io.Serializable;
import java.util.ArrayList;

import enums.PizzaIngredient;
import enums.PizzaMass;
import enums.PizzaSize;

public class Pizza implements Serializable {

	private PizzaSize size;
	private PizzaMass mass;
	private ArrayList<PizzaIngredient> ingredients;
	private static final long serialVersionUID = 3804841188221182014L;
	
	public Pizza() {
		this(null, null, new ArrayList<>());
	}

	public Pizza(PizzaSize size, PizzaMass mass, ArrayList<PizzaIngredient> ingredients) {
		this.setSize(size);
		this.setMass(mass);
		this.setIngredients(ingredients);
	}

	public PizzaSize getSize() {
		return size;
	}

	public void setSize(PizzaSize size) {
		this.size = size;
	}

	public PizzaMass getMass() {
		return mass;
	}

	public void setMass(PizzaMass mass) {
		this.mass = mass;
	}

	public ArrayList<PizzaIngredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<PizzaIngredient> ingredient) {
		this.ingredients = ingredient;
	}

	@Override
	public String toString() {
		return "Pizza [size=" + size + ", mass=" + mass + ", ingredient=" + ingredients + "]";
	}
	
}
