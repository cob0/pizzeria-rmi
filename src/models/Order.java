package models;

import java.io.Serializable;

public class Order implements Serializable {

	private String name;
	private String street;
	private String contact;
	private Pizza pizza;
	private static final long serialVersionUID = -5394164215348060960L;
	
	public Order() {
		this("", "", "", new Pizza());
	}

	public Order(String name, String street, String contact, Pizza pizza) {
		this.setName(name);
		this.setStreet(street);
		this.setContact(contact);
		this.setPizza(pizza);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}

	@Override
	public String toString() {
		return "Order [name=" + name + ", street=" + street + ", contact=" + contact + ", pizza=" + pizza + "]";
	}
	
}
